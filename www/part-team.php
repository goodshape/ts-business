<div class="main-wrapper p2 flex items-center">
	<div class="content">
		<h2 class="h1">
			O našem týmu
		</h2>
		<p>Náš tým tvoří odborníci na digitální technologie a aplikace, a především nadšení kulturní fanoušci. Objíždíme hrady, zámky, muzea i galerie po celé České republice, poznáváme naše kulturní dědictví a hledáme způsob, jak jej pomoci předat co nejširšímu publiku.</p>
		<p>Známe situace, ve kterých se správci kulturního dědictví ocitají, a pomáháme pro ně vyvíjet moderní řešení přímo na míru. </p>
		<div class="flex">
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Audioprůvodce</div>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Mapa</div>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Příběhy na míru</div>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Interaktivita</div>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">3D</div>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Rozšíření reality</div>
			</div>
		</div>
	</div>
</div>