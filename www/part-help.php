<div class="main-wrapper p2 flex items-center">
	<div class="slider">
		
	</div>
	<div class="content">
		<h2 class="h1">
			Tour stories vám umožní
		</h2>
		<div class="flex">
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Řešení pro moderního návštěvníka </div>
				<p>Moderní návštěvníci mají čím dál více požadavků a potřeb. Nechtějí čekat, bojí se používat audio průvodce po někom jiném, chtěli by vidět, jak to tu vypadalo ve 14. století...  S námi najdou odpovědi i možnost individuální plnohodnotné prohlídky kdykoliv přímo ve své kapse a svém vlastním telefonu</p>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Lidské kapacity</div>
				<p>Víme, že je složité plánovat kapacity průvodců, nebo mít prohlídky v každém jazyce návštěvníka. TourStories vám pomohou s doplněním těchto kapacit, abyste měli ještě spokojenější návštěvníky - a hlavně i zaměstnance. Nenahrazujeme lidské průvodce, doplňujeme je.</p>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Rozšíření návštěvnického zážitku</div>
				<p>Ať byste rádi svým návštěvníkům ukázali každičký detail, na který na prohlídkách nezbývá čas, nebo je nemůžete z nějakého důvodu pustit za zavřené dveře, v TourStories na to prostor i možnosti máme. </p>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Statistiky</div>
				<p>S TourStories budete mít vždy přesný přehled, jaké expozice návštěvníky přimějí se nejdéle zastavit, v jakých jazycích a kdy se na prohlídku vydávají, ale i třeba, že ten krátký příběh o tom, jak se tu kdysi zastavil Karel IV., zajímá více lidí, než jste si možná mysleli. </p>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Ušetření nákladů</div>
				<p>Audioprůvodci se rádi "ztrácí", i "rozbíjejí", a nyní je přímo nutné je neustále dezinfikovat. Víme, že i náklady na tištěné průvodce a mapky dokážou být nepříjemné. S platformou přímo v telefonu návštěvníků se o nic takového starat nemusíte. </p>
			</div>
			<div class="slide-nav">
				<img src="" alt="">
				<div class="text">Zapojení do společné platformy</div>
				<p>Nemusíte vyvíjet žádnou svoji vlastní aplikaci. TourStories spojuje příběhy z nádherných míst po celé republice, a tak už vás třeba budou mít návštěvníci rovnou stažené. A kdo ví - možná si při prohlídce nedalekého muzea všimnou, že kousek od nich se nachází váš zámek, a rozhodnou se jej vydat také prozkoumat. </p>
			</div>
		</div>
	</div>
</div>