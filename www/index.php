<!-- Stored in resources/views/layouts/master.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- metas -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
    <!-- /metas -->
    <!-- styles -->
    	<!-- reset -->
    	<link rel="stylesheet" href="https://use.typekit.net/xnc8tgl.css">
    	<style>a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}*{box-sizing: border-box;}</style>
    	<!-- /reset -->
        <link rel="shortcut icon" href="/favicon.ico">
        <link href="/css/bass.css" rel="stylesheet">
        <link href="/css/main.css" rel="stylesheet">
        
    <!-- /styles -->
    <!-- head -->
    <!-- /head -->
    <title>GoodShape FrontBase</title>
    <script> 
    var $buoop = {vs:{i:11,f:35,o:12.1,s:7},c:2}; 
    function $buo_f(){ 
     var e = document.createElement("script"); 
     e.src = "//browser-update.org/update.min.js"; 
     document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}
    </script>
</head>
<body>
    <header class="full-width site-header blurredbg fixed p2 z4 ">
    	<div class="master-wrap mx-auto px2 flex items-center justify-between">
	        <div class="head left pr2">
	        	<img src="/img/logo.svg" width="" class="site-logo" alt="">
	            <h2 class="hide site-title">GoodShape FrontBase</h2>
	        </div>
	        <input type="checkbox" id="hamburger" name="hamburger">
	        <label for="hamburger" class="hamburger right"><span></span></label>
	        <nav class="mobile-nav nav blurredbg center
	        p2">
	            <?php include("./parts-nav.php"); ?>
	        </nav>
    	</div>
    </header>
    <?php include("./part-head.php") ?>
    <?php include("./part-possibilities.php") ?>
    <?php include("./part-find.php") ?>
    <?php include("./part-testimonials.php") ?>
    <?php include("./part-help.php") ?>
    <?php include("./part-testimonials.php") ?>
    <?php include("./part-team.php") ?>
    <?php include("./part-cooperate.php") ?>
    <?php include("./part-form.php") ?>
    <div class="container">
        
    </div>
    <footer class="site-footer">
        <div class="master-padding">
            <div class="copy">
                &copy; <?= date("Y"); ?> - Goodshape
            </div>
            <div class="foomenu">
                <?php include("./parts-nav.php"); ?>
            </div>
        </div>
    </footer>
    <script src="/js/app.min.js"></script>
</body>
</html>