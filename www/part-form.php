<div class="main-wrapper p2 items-center">
	<h2 class="h1">
		O našem týmu
	</h2>
	<div class="flex">
		<div class="content">
			<p>Spojte se s námi a začněme společnou cestu. Rádi si popovídáme, ať už máte jakékoliv otázky, nebo vás o TourStories či možnostech přímo pro vás zajímá něco více. Lukáš, Kryštof nebo Petr s vámi spojí v nejkratší možné době. </p>
		</div>			
		<div class="form">
			<div class="flex">
				<div class="input-wrap">
					<label for="name">Vaše jméno</label>
					<input type="text">				
				</div>
				<div class="input-wrap">
					<label for="name">Váš email</label>
					<input type="email">				
				</div>
			</div>
			<div class="flex">
				<div class="input-wrap">
					<label for="name">Vaše jméno</label>
					<input type="text">				
				</div>
				<div class="input-wrap">
					<label for="name">Váš email</label>
					<input type="email">				
				</div>
			</div>
			<div class="flex">
				<div class="input-wrap">
					<label for="name">Vaše jméno</label>
					<textarea type="text">				
				</div>
			</div>
			<button class="button">Odeslat</button>
		</div>
	</div>
</div>