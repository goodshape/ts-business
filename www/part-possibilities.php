<div class="main-wrapper p2 flex items-center">
	<div class="slider">
		
	</div>
	<div class="content">
		<h2 class="h1">
			Tour stories vám umožní
		</h2>
		<div class="flex">
			<div class="slide-nav">
				<img src="./img/icon-pruvodce.jpg" alt="">
				<div class="text">Audioprůvodce</div>
			</div>
			<div class="slide-nav">
				<img src="./img/icon.mapa.jpg" alt="">
				<div class="text">Mapa</div>
			</div>
			<div class="slide-nav">
				<img src="./img/icon-story.jpg" alt="">
				<div class="text">Příběhy na míru</div>
			</div>
			<div class="slide-nav">
				<img src="./img/icon-interactivity.jpg" alt="">
				<div class="text">Interaktivita</div>
			</div>
			<div class="slide-nav">
				<img src="./img/icon-3d.jpg" alt="">
				<div class="text">3D</div>
			</div>
			<div class="slide-nav">
				<img src="./img/icon-ar.jpg" alt="">
				<div class="text">Rozšíření reality</div>
			</div>
		</div>
	</div>
</div>