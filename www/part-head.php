<div class="hp-header overflow-hidden">
	<div class="bg-holder vh100 overflow-hidden full-width absolute">
		<img src="./img/hp-bg.jpg" class="fill" alt="">		
	</div>
	<div class="flex vh100 items-center master-wrap mx-auto">		
		<div class="header-content z2 relative pl2 pr2">
			<img src="./img/bgleaf.png" class="leaf-bg absolute" alt="">
			<div class="max-width-1 z2 relative">	
				<img src="./img/logo.svg" class="mt4 intrologo" alt="">
				<div class="introtext">
					<p>Digitální průvodce v&nbsp;telefonech vašich návštěvníků.</p>
					<p>Pomáháme podpořit  genius loci hradů, zámků, muzeí, galerií i&nbsp;měst, a&nbsp;rozšířit zážitek z&nbsp;každé návštěvy. </p>
					<button class="button px2 py1 pb2">Chci vědět více</button>
				</div>	
			</div>
		</div>
	</div>
</div>