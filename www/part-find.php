<div class="main-wrapper p2 flex items-center">
	<div class="content">
		<h2 class="h1">
			V platformě TourStories již můžete objevovat:
		</h2>
		<div class="flex">
			<div class="slide-nav">
				<img src="./img/logo-ntm.png" alt="">
				<div class="text">Národní Technické Muzeum</div>
			</div>
			<div class="slide-nav">
				<img src="./img/logo-veveri.png" alt="">
				<div class="text">Hrad Veveří</div>
			</div>
			<div class="slide-nav">
				<img src="./img/logo-krumlov.png" alt="">
				<div class="text">Hrad a&nbsp;zámek Český Krumlov</div>
			</div>
			<div class="slide-nav">
				<img src="./img/logo-muo.png" alt="">
				<div class="text">Muzeum umění Olomouc</div>
			</div>
			<div class="slide-nav">
				<img src="./img/logo-zetor.png" alt="">
				<div class="text">Zetor Gallery</div>
			</div>
		</div>
		<div class="center">... a další</div>
	</div>
	<div class="image">
		<img src="" alt="">
	</div>
</div>